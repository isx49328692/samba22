# Samba
FROM debian:latest
LABEL version="1.0"
LABEL author="@juan22"
LABEL subject="samba server"
RUN apt-get update
ARG DEBIAN_FRONTED=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libpam-ldapd libnss-ldapd nslcd nslcd-utils ldap-utils openssh-client openssh-server samba samba-client cifs-utils
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
